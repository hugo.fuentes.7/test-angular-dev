import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RegistrarProductoService } from './registrar-producto.service';

describe('ApiIntegrationService', () => {
  let service: RegistrarProductoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(RegistrarProductoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
