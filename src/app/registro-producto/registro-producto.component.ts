import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { RegistrarProductoService } from './service/registrar-producto.service';

import { AbstractControl, AsyncValidatorFn, FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, catchError, map, of } from 'rxjs';

@Component({
  selector: 'app-registro-producto',
  templateUrl: './registro-producto.component.html',
  styleUrls: ['./registro-producto.component.scss']
})
export class RegistroProductoComponent {

  @Output() cerrarRegistroEvent = new EventEmitter<void>();

  formRegistro!: FormGroup; // | undefined;
  logoDefault = 'https://www.visa.com.ec/dam/VCOM/regional/lac/SPA/Default/Pay%20With%20Visa/Tarjetas/visa-signature-400x225.jpg';
  existId: any = false;

  constructor(private fb: FormBuilder,
              private registrarProductoService: RegistrarProductoService) {

    this.formRegistro = this.initForm();

    this.formRegistro.get('id')?.valueChanges.subscribe( value => {
      if(this.formRegistro.get('id')?.value.length >0) {
        this.existsProductById();
      }
    });
  }

  //
  initForm(): FormGroup {
    return this.fb.group({
      id:           ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10) ]],
      name:         ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
      description:  ['', [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
      logo:         [this.logoDefault, [Validators.required]],
      date_release: ['', [Validators.required,
                          Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/),
                          this.validatorDateRealease ]],
      date_revision: [ {value: '', disabled:true},
                       [Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/) ]]
    });
  }

  //
  async registrarProducto() {
    this.validateFields();
    if(this.formRegistro.invalid)
      return;

    let partes = this.formRegistro?.get('date_release')?.value?.split('/');
    let fecha1 = new Date(parseInt(partes[2], 10), parseInt(partes[1], 10) - 1, parseInt(partes[0], 10));

     partes = this.formRegistro?.get('date_revision')?.value?.split('/');
     let fecha2 = new Date(parseInt(partes[2], 10), parseInt(partes[1], 10) - 1, parseInt(partes[0], 10));

    let producto = {
      id: this.formRegistro.get('id')?.value,
      name: this.formRegistro.get('name')?.value,
      description: this.formRegistro.get('description')?.value,
      logo: this.formRegistro.get('logo')?.value,
      date_release: fecha1, //this.formRegistro.get('date_release')?.value,
      date_revision: fecha2, //tmpTxt,
        };

    this.registrarProductoService.setProducto(producto).subscribe( {
      next: (v) => {
        //alert('New Product has been Created');
        alert('Se ha creado un nuevo producto');
        this.cancel();
      },
      error: (e) => {
        if(e.status == 400) {
          alert(e.error);
        }
      },
      complete: () => {
        console.log("Completo");
      }
    });
  }

  existsProductById(): void {
    const id = this.formRegistro.get('id')?.value;
    if( /*this.formRegistro?.get('id')?.touched &&*/ this.formRegistro.get('id')?.hasError('minlength')  == false && this.formRegistro.get('id')?.hasError('maxlength') == false) {
      this.registrarProductoService.existsProductById(id).subscribe( {
        next: (v) => {
          //console.log("V", v);
          this.existId = v;
        },
        error: (e) => {
          alert(e.error);
          //console.log('ERROR:', e)
          this.existId = true;
        },
      });
      console.log("this.existId...", this.existId);
    }
  }


  cancel() {
    this.cerrarRegistroEvent.emit();
  }

  validateFields() {
    this.markAllAsTouhed(this.formRegistro);
  }

  // Form
  restartForm(): void {
    this.formRegistro.reset();
  }

  calculateDateRevision() {
    if(this.formRegistro?.get('date_release')?.value) {
      var partes = this.formRegistro?.get('date_release')?.value.split('/');
      var fechaObjeto = new Date(parseInt(partes[2], 10), parseInt(partes[1], 10) - 1, parseInt(partes[0], 10));

      let dateRevision = new Date();
      dateRevision = fechaObjeto;
      dateRevision.setFullYear(fechaObjeto.getFullYear() + 1);

      let tmpTxt = (dateRevision.getDate()+1).toString().padStart(2, '0') + '/' +
                  (dateRevision.getMonth()+1).toString().padStart(2, '0') + '/' +
                  dateRevision.getFullYear();
      this.formRegistro?.get('date_revision')?.setValue(tmpTxt);
    }
  }

  private markAllAsTouhed(control: AbstractControl) {
    if (control instanceof FormGroup || control instanceof FormArray) {
      Object.values(control.controls).forEach(c => this.markAllAsTouhed(c));
    } else {
      control.markAsTouched();
    }
  }

  // Validators
  validatorDateRealease(control: AbstractControl): { [key: string]: boolean } | null {
    if(control?.value) {
      let partes = control.value.split('/');
      let fechaObjeto = new Date(parseInt(partes[2], 10), parseInt(partes[1], 10) - 1, parseInt(partes[0], 10));

      const fecha = new Date(fechaObjeto);
        if(isNaN(fecha.getTime())) {
          return { 'fecha no válida': true };
        }
        const fechaActual = new Date();
        if(fecha <= fechaActual) {
          return { 'fecha debe ser mayor a actual': true };
        }
    }
    return null;
  }

  //
  // GET VALIDATORS
  //

  // ID
  get isInvalidId() {
    return ( this.existId || (this.formRegistro?.get('id')?.touched && this.formRegistro.get('id')?.errors) )? true : false;
  }
  get isInvalidIdByExistence(){
    return ( /*this.formRegistro?.get('id')?.touched &&*/ this.existId )? true : false;
  }
  get isInvalidIdByRequired() {
    return ( this.formRegistro?.get('id')?.touched && this.formRegistro.get('id')?.hasError('required') )? true : false;
  }
  get isInvalidIdByMinLength() {
    return ( this.formRegistro?.get('id')?.touched && this.formRegistro.get('id')?.hasError('minlength') )? true : false;
  }
  get isInvalidIdByMaxLength() {
    return ( this.formRegistro?.get('id')?.touched && this.formRegistro.get('id')?.hasError('maxlength') )? true : false;
  }

  // NAME
  get isInvalidName() {
    return (this.formRegistro?.get('name')?.touched && this.formRegistro.get('name')?.errors) ? true : false;
  }
  get isInvalidNameByRequired() {
    return ( this.formRegistro?.get('name')?.touched && this.formRegistro.get('name')?.hasError('required') )? true : false;
  }
  get isInvalidNameByMinLength() {
    return ( this.formRegistro?.get('name')?.touched && this.formRegistro.get('name')?.hasError('minlength') )? true : false;
  }
  get isInvalidNameByMaxLength() {
    return ( this.formRegistro?.get('name')?.touched && this.formRegistro.get('name')?.hasError('maxlength') )? true : false;
  }

  // DESCRIPTION
  get isInvalidDescription() {
    return (this.formRegistro?.get('description')?.touched && this.formRegistro.get('description')?.errors) ? true : false;
  }
  get isInvalidDescriptionByRequired() {
    return ( this.formRegistro?.get('description')?.touched && this.formRegistro.get('description')?.hasError('required') )? true : false;
  }
  get isInvalidDescriptionByMinLength() {
    return ( this.formRegistro?.get('description')?.touched && this.formRegistro.get('description')?.hasError('minlength') )? true : false;
  }
  get isInvalidDescriptionByMaxLength() {
    return ( this.formRegistro?.get('description')?.touched && this.formRegistro.get('description')?.hasError('maxlength') )? true : false;
  }


  get isInvalidLogo() {
    return (this.formRegistro?.get('logo')?.touched && this.formRegistro.get('logo')?.errors) ? true : false;
  }

  get isInvalidDateRelease() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.errors) ? true : false;
  }
  get isInvalidDateReleaseByRequired() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('required')) ? true : false;
  }
  get isInvalidDateReleaseByFormat() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('pattern')) ? true : false;
  }
  get isInvalidDateReleaseByGreater() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('validatorDateRealease')) ? true : false;
  }

  get isInvalidDateRevision() {
    return (this.formRegistro?.get('date_revision')?.touched && this.formRegistro.get('date_revision')?.errors) ? true : false;
  }

}
