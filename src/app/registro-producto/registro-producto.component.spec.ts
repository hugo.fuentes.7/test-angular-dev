import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RegistroProductoComponent } from './registro-producto.component';

describe('RegistroProductoComponent', () => {
  let component: RegistroProductoComponent;
  let fixture: ComponentFixture<RegistroProductoComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegistroProductoComponent],
      declarations: [ RegistroProductoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistroProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  test('Click en boton Enviar', () => {
    const button: HTMLButtonElement | null = compiled.querySelector('.send-button');
    button?.click();

    const divErrors = compiled.querySelectorAll('.error-field');
    expect(divErrors.length).toBeLessThanOrEqual(0);
  });

});
