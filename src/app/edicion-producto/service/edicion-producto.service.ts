import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EdicionProductoService {

  URLBASE = environment.URLBASE;
  AUTHORID = environment.authorId;

  constructor(private http: HttpClient) { }

  updateProduct(jsonBody: any): Observable<any> {
    let url = `${this.URLBASE}/bp/products`;
    const options = this.getOptions();
    return this.http.put(url, jsonBody, options);
  }


  getOptions() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorId': this.AUTHORID
    });
    return { headers: headers };
  }

}
