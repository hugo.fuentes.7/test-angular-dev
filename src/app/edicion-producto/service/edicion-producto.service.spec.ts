import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';


import { EdicionProductoService } from './edicion-producto.service';

describe('EdicionProductoService', () => {
  let service: EdicionProductoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      //providers: [HttpClientTestingModule],
    });
    service = TestBed.inject(EdicionProductoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
