import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EdicionProductoService } from './service/edicion-producto.service';

@Component({
  selector: 'app-edicion-producto',
  templateUrl: './edicion-producto.component.html',
  styleUrls: ['./edicion-producto.component.scss']
})
export class EdicionProductoComponent {

  @Output() cerrarEdicionEvent = new EventEmitter<void>();
  @Output() edicionCompletadaEvent = new EventEmitter<void>();
  @Input() producto!: any;
  formRegistro!: FormGroup;

  constructor(private http: HttpClient,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private edicionProdutoService: EdicionProductoService) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['producto'] && changes['producto'].currentValue) {
      this.formRegistro = this.initForm();
    }
  }

  cancel() {
    this.cerrarEdicionEvent.emit();
  }

  //
  initForm(): FormGroup {
    const date1 = this.transformDateToStringFormat(this.producto.date_release);
    const date2 = this.transformDateToStringFormat(this.producto.date_revision);

    return this.fb.group({
      id: [ {value: this.producto.id, disabled:true}, [Validators.required, Validators.minLength(3), Validators.maxLength(10) ]],
      name: [this.producto?.name, [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
      description: [this.producto.description, [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
      logo: [this.producto.logo, [Validators.required]],
      date_release: [date1, [Validators.required,
                          Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/),
                          /*this.validatorDateRealease*/ ]],
      date_revision: [ { value: date2, disabled: true },
                       [Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/) ]]
    });
  }

  updateProduct() {
    const fecha1 = this.transformStringToDateFormat(this.formRegistro.get('date_release')?.value);
    const fecha2 = this.transformStringToDateFormat(this.formRegistro.get('date_revision')?.value);

    let producto = {
      id: this.formRegistro.get('id')?.value,
      name: this.formRegistro.get('name')?.value,
      description: this.formRegistro.get('description')?.value,
      logo: this.formRegistro.get('logo')?.value,
      date_release: fecha1,
      date_revision: fecha2,
        };

    this.edicionProdutoService.updateProduct(producto).subscribe( {
      next:(v) => {
        console.log(v);
        //this.edicionCompletadaEvent.emit();
      },
      error: (e) => {
        alert(e.error);
        //console.log(e);
      },
      complete: () => {
        console.log("complete");
        this.edicionCompletadaEvent.emit();
      }
    });
  }

  transformStringToDateFormat(textDate: string) {
    let  parts = textDate.split('/');
    let fecha2 = new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10));
    return fecha2;
  }

  transformDateToStringFormat(dateOrigin: any) {
    const date = new Date(dateOrigin);
    const dia = ('0' + date.getDate()).slice(-2); // Rellenar con cero y obtener los últimos dos caracteres
    const mes = ('0' + (date.getMonth() + 1)).slice(-2); // Rellenar con cero y obtener los últimos dos caracteres
    const año = date.getFullYear();

    const fechaFormateada = `${dia}/${mes}/${año}`;

    return fechaFormateada;
  }

  //
  calculateDateRevision() {
    if(this.formRegistro?.get('date_release')?.value) {
      let dateRelease = new Date(this.formRegistro?.get('date_release')?.value);

      var partes = this.formRegistro?.get('date_release')?.value.split('/');
      var fechaObjeto = new Date(parseInt(partes[2], 10), parseInt(partes[1], 10) - 1, parseInt(partes[0], 10));

      let dateRevision = new Date();

      dateRevision = fechaObjeto;
      dateRevision.setFullYear(fechaObjeto.getFullYear() + 1);

      let tmpTxt = (dateRevision.getDate()+1).toString().padStart(2, '0') + '/' +
                  (dateRevision.getMonth()+1).toString().padStart(2, '0') + '/' +
                  dateRevision.getFullYear();

      this.formRegistro?.get('date_revision')?.setValue(tmpTxt);
    }

  }



  //
  get isInvalidId(){
    return (this.formRegistro?.get('id')?.touched && this.formRegistro.get('id')?.errors) ? true : false;
  }

  get isInvalidName() {
    return (this.formRegistro?.get('name')?.touched && this.formRegistro.get('name')?.errors) ? true : false;
  }

  get isInvalidDescription() {
    return (this.formRegistro?.get('description')?.touched && this.formRegistro.get('description')?.errors) ? true : false;
  }

  get isInvalidLogo() {
    return (this.formRegistro?.get('logo')?.touched && this.formRegistro.get('logo')?.errors) ? true : false;
  }

  get isInvalidDateRelease() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.errors) ? true : false;
  }
  get isInvalidDateReleaseByRequired() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('required')) ? true : false;
  }
  get isInvalidDateReleaseByFormat() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('pattern')) ? true : false;
  }
  get isInvalidDateReleaseByGreater() {
    return (this.formRegistro?.get('date_release')?.touched && this.formRegistro.get('date_release')?.hasError('validatorDateRealease')) ? true : false;
  }

  get isInvalidDateRevision() {
    return (this.formRegistro?.get('date_revision')?.touched && this.formRegistro.get('date_revision')?.errors) ? true : false;
  }

}
