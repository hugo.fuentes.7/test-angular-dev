import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ListaProductosComponent } from './lista-productos.component';

describe('ListaProductosComponent', () => {
  let component: ListaProductosComponent;
  let fixture: ComponentFixture<ListaProductosComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ListaProductosComponent],
      declarations: [ ListaProductosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListaProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  test('Click en Boton Agregar', () => {
    const buttons = compiled.querySelectorAll('.button-agregar');
    const button: HTMLButtonElement | null = compiled.querySelector('.button-agregar');
    button?.click();

    expect( component.showRegistro ).toBe(true);
    expect( component.showListComponent ).toBe(false);
  });

});
