import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListaProductosService {

  URLBASE = environment.URLBASE;
  AUTHORID = environment.authorId;

  constructor(private http: HttpClient) { }

  getProductos(): Observable<any> {
    let url = `${this.URLBASE}/bp/products`;
    const options = this.getOptions();
    return this.http.get(url, options);
  }

  getOptions() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorId': this.AUTHORID
    });
    return { headers: headers };
  }

}
