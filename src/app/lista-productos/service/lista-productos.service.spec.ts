import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ListaProductosService } from './lista-productos.service';

describe('ListaProductosService', () => {
  let service: ListaProductosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ListaProductosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
