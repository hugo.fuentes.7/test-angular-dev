import { Component, OnInit } from '@angular/core';
import { ListaProductosService } from './service/lista-productos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss']
})
export class ListaProductosComponent implements OnInit {

  listOfProducts: any[] = [];
  listOfProductsOnPage: any[] =[];
  productSelected: any = {};

  showListComponent = true;
  showEliminacion = false;
  showEdicion = false;
  showRegistro = false;

  searchingText: string = '';


  resultsOnPage = 5;

  constructor(private listaProductosService: ListaProductosService,
              ) {



  }


  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts() {
    let  productos$: any;
    productos$ = this.listaProductosService.getProductos().subscribe ( {
      next: (result) => {
        this.listOfProducts = result;
        this.updateListOnPage();
        //console.log(result);
      },
      error: (e) => {
        console.log(e);
      }
    });
  }




 searchProduct() {
  if(this.searchingText == null || this.searchingText == '') {
    this.updateListOnPage();
  } else {
    const tmpText = this.searchingText.toUpperCase();
    this.listOfProductsOnPage = this.listOfProductsOnPage.filter(item => item.name.toUpperCase().includes(tmpText) ||
                                           item.description.toUpperCase().includes(tmpText)
    );
  }
 }

 get numberOfResults() {
  if(this.listOfProducts == null || this.listOfProducts.length == 0)
    return 0;
  else
    return this.listOfProducts.length;
 }

  changeResultsOnPage(event: any) {
    this.resultsOnPage = event.target.value;
    this.updateListOnPage();
  }

  updateListOnPage() {
    this.listOfProductsOnPage = this.listOfProducts.slice(0, this.resultsOnPage);
  }

  goToAdd() {
    this.showRegistro = true;
    this.showListComponent = false;
  }

  goToEdit(item:any) {
    this.productSelected = item;
    this.showEdicion = true;
    this.showListComponent = false;
  }

  goToDelete(item: any) {
    this.productSelected = item;
    this.showEliminacion = true;
  }


  goToList() {
    this.showListComponent = true;
  }

  closeEdicion() {
    this.showEdicion = false;
    this.showListComponent = true;
    this.loadProducts();
    this.updateListOnPage();
  }

  closeRegistro() {
    this.showRegistro = false;
    this.showListComponent = true;
    this.loadProducts();
    this.updateListOnPage();
  }

  closeEliminacion() {
    this.showEliminacion = false;
    this.showListComponent = true;
    this.loadProducts();
    this.updateListOnPage();
  }

  showInfo() {
    alert('Información');
  }

}
