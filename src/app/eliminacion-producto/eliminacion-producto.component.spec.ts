import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminacionProductoComponent } from './eliminacion-producto.component';
import { EliminacionProductoService } from './service/eliminacion-producto.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('EliminacionProductoComponent', () => {
  let component: EliminacionProductoComponent;
  let fixture: ComponentFixture<EliminacionProductoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EliminacionProductoService],
      declarations: [ EliminacionProductoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EliminacionProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
