import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EliminacionProductoService } from './service/eliminacion-producto.service';

@Component({
  selector: 'app-eliminacion-producto',
  templateUrl: './eliminacion-producto.component.html',
  styleUrls: ['./eliminacion-producto.component.scss']
})
export class EliminacionProductoComponent {

  //@Output() cerrar = new EventEmitter<void>();
  @Output() eliminacionCompletadaEvent = new EventEmitter<void>();
  @Input() producto: any;

  constructor(private eliminacionProductoService: EliminacionProductoService) {

  }

  cerrarModal() {
    this.eliminacionCompletadaEvent.emit();
  }

  detenerPropagacion(event: Event): void {
    event.stopPropagation();
  }

  deleteProduct() {
    this.eliminacionProductoService.deleteProduct(this.producto.id).subscribe( {
      next: (v) => {
        //console.log('v', v);
        this.cerrarModal();
      },
      error: (e) => {
         console.table(e);
        if(e.status == 400 || e.status == 404)
          alert(e.status + ', ' + e.error);
        if(e.status == 200) {
          //alert(e.error.text);
          this.cerrarModal();
        }


      },
      complete: () => {

      }
    })
  }

}
