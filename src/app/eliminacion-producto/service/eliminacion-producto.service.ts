import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EliminacionProductoService {

  URLBASE = environment.URLBASE;
  AUTHORID = environment.authorId;

  constructor(private http: HttpClient) { }


  deleteProduct(id: string): Observable<any> {
    let url = `${this.URLBASE}/bp/products?id=${id}`;
    const options = this.getOptions();
    return this.http.delete(url, options);
  }

  getOptions() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorId': this.AUTHORID
    });
    return { headers: headers };
  }

}
