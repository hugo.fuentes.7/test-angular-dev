import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
//import { HttpClient } from '@angular/common/http';

import { EliminacionProductoService } from './eliminacion-producto.service';


describe('EliminacionProductoService', () => {
  let service: EliminacionProductoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ],
      providers: [HttpClientTestingModule, ]
    });
    service = TestBed.inject(EliminacionProductoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
