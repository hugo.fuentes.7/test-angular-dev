import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { RegistroProductoComponent } from './registro-producto/registro-producto.component';
import { EliminacionProductoComponent } from './eliminacion-producto/eliminacion-producto.component';
import { ListaProductosService } from './lista-productos/service/lista-productos.service';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EdicionProductoComponent } from './edicion-producto/edicion-producto.component';

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    ListaProductosComponent,
    RegistroProductoComponent,
    EliminacionProductoComponent,
    EdicionProductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es' },
                ListaProductosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
