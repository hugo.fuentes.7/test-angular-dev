import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistroProductoComponent } from './registro-producto/registro-producto.component';
import { EdicionProductoComponent } from './edicion-producto/edicion-producto.component';
import { EliminacionProductoComponent } from './eliminacion-producto/eliminacion-producto.component';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';

const routes: Routes = [
  { path: 'registro-producto', component: RegistroProductoComponent },
  { path: 'edicion-producto', component: EdicionProductoComponent },
  //{ path: 'edicion-producto/:producto', component: EdicionProductoComponent },
  { path: 'eliminacion-producto', component: EliminacionProductoComponent },
  { path: 'lista-productos', component: ListaProductosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
